%global commit 8731689ccc9877df365b502d4f512ccf9d79d9f3
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           mt32emu-qt
Version:        1.10.0
Release:        1.20211110git%{shortcommit}%{?dist}
Summary:        A software synthesiser emulating e.g. the Roland MT-32

License:        GPLv3
URL:            https://github.com/munt/munt
Source0:        https://github.com/munt/munt/archive/%{commit}/munt-%{shortcommit}.tar.gz

BuildRequires:  cmake
BuildRequires:  gcc-c++
BuildRequires:  pkgconfig(Qt5Widgets)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(portaudio-2.0)
BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(jack)
BuildRequires:  pkgconfig(mt32emu)
BuildRequires:  desktop-file-utils

%description
The main synthesiser application that is a part of the Munt project. It makes use of the mt32emu library and the Qt framework. It facilitates both realtime synthesis and conversion of pre-recorded Standard MIDI files to WAVE files.

%prep
%autosetup -p1 -n munt-%{commit}

%build
cd mt32emu_qt
%cmake .
%cmake_build

%install
cd mt32emu_qt
%cmake_install
desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop

%files
%doc %{_docdir}/munt/%{name}/{AUTHORS.txt,NEWS.txt,README.md,TODO.txt}
%license %{_docdir}/munt/%{name}/COPYING.txt
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/munt*.*
%{_bindir}/%{name}

%changelog
* Wed Nov 10 2021 spike <spike@fedoraproject.org> 1.10.0-1.20211110git8731689
- Initial package
